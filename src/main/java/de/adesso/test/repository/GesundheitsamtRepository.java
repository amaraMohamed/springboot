package de.adesso.test.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import de.adesso.test.entities.Gesundheitsamt;

public interface GesundheitsamtRepository extends CrudRepository<Gesundheitsamt, Long> {
 List<Gesundheitsamt> findByName(String name);
}
