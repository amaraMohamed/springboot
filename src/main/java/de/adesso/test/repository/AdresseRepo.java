package de.adesso.test.repository;

import org.springframework.data.repository.CrudRepository;

import de.adesso.test.entities.Adresse;

public interface AdresseRepo extends CrudRepository<Adresse, Long> {

}
