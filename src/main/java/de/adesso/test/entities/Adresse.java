package de.adesso.test.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Adresse {

	@Id
	@GeneratedValue
	@Column(unique = true)
	private long adresseId;
	
	private String strasse;
	private String hausnr;
	private String ort;
	private String plz;
	public long getAdresseId() {
		return adresseId;
	}
	public void setAdresseId(long adresseId) {
		this.adresseId = adresseId;
	}
	public String getStrasse() {
		return strasse;
	}
	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}
	public String getHausnr() {
		return hausnr;
	}
	public void setHausnr(String hausnr) {
		this.hausnr = hausnr;
	}
	public String getOrt() {
		return ort;
	}
	public void setOrt(String ort) {
		this.ort = ort;
	}
	public String getPlz() {
		return plz;
	}
	public void setPlz(String plz) {
		this.plz = plz;
	}
	public Adresse(long adresseId, String strasse, String hausnr, String ort, String plz) {
		super();
		this.adresseId = adresseId;
		this.strasse = strasse;
		this.hausnr = hausnr;
		this.ort = ort;
		this.plz = plz;
	}
	public Adresse(String strasse, String hausnr, String ort, String plz) {
		this.strasse = strasse;
		this.hausnr = hausnr;
		this.ort = ort;
		this.plz = plz;
	}
	
	public Adresse() {
		
	}
	
	
}
