package de.adesso.test.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Gesundheitsamt {

	@Id
	@GeneratedValue
	@Column(unique = true)
	private long gesundheitsamtId;
	
	private String name;
	private String telefonnummer;
	@Column(length = 3000)
	private String email;
	@OneToMany
	private List<Adresse> adresse;
	
	
	public List<Adresse> getAdresse() {
		return adresse;
	}
	public void setAdresse(List<Adresse> adresse) {
		this.adresse = adresse;
	}
	public long getGesundheitsamtId() {
		return gesundheitsamtId;
	}
	public void setGesundheitsamtId(long gesundheitsamtId) {
		this.gesundheitsamtId = gesundheitsamtId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTelefonnummer() {
		return telefonnummer;
	}
	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Gesundheitsamt(long gesundheitsamtId, String name, String telefonnummer,
			String email, List<Adresse> adresse) {
		super();
		this.gesundheitsamtId = gesundheitsamtId;
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.email = email;
		this.adresse = adresse;
	}	
	
	public Gesundheitsamt(String name, String telefonnummer,
			String email, List<Adresse> adresse) {
		super();
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.email = email;
		this.adresse = adresse;
	}
	
	public Gesundheitsamt() {
		
	}
}
