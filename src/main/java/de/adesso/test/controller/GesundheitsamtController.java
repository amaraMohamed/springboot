package de.adesso.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

import de.adesso.test.entities.Adresse;
import de.adesso.test.entities.Gesundheitsamt;
import de.adesso.test.interfaces.GesundheitsamtInterface;

@RestController
public class GesundheitsamtController {

	@Autowired
	GesundheitsamtInterface gesundheitsamtService;
	
	@GetMapping("/add")
	public String add() {
		Adresse adresse = new Adresse("Souk Jemaa", "10A", "Tripoli", "10000");
		Gesundheitsamt gesundheitsamt = new Gesundheitsamt("Mahmoud", "+2164587999", "email@mahmoud.lyb", 
				Lists.newArrayList(adresse));
		gesundheitsamtService.save(gesundheitsamt);
		return "Erledigt ;)";
	}
	
	@PostMapping("")
	public ResponseEntity<Gesundheitsamt> save(@RequestBody Gesundheitsamt gesundheitsamt) {
		return gesundheitsamtService.save(gesundheitsamt);
	}
	
	@GetMapping("")
	public List<Gesundheitsamt> getAll() {
		return gesundheitsamtService.getAll();
	}
	
	@PutMapping("")
	public ResponseEntity<Gesundheitsamt> update(@RequestBody Gesundheitsamt gesundheitsamt) throws Throwable {
		return gesundheitsamtService.update(gesundheitsamt);
	}
	
	@GetMapping("/{name}")
	public List<Gesundheitsamt> findByName(@PathVariable String name) {
		return gesundheitsamtService.findByName(name);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable long id) {
		return gesundheitsamtService.delete(id);
	}
}
