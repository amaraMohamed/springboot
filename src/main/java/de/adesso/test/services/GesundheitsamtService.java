package de.adesso.test.services;

//import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import de.adesso.test.entities.Adresse;
import de.adesso.test.entities.Gesundheitsamt;
import de.adesso.test.interfaces.GesundheitsamtInterface;
import de.adesso.test.repository.AdresseRepo;
import de.adesso.test.repository.GesundheitsamtRepository;

@Service
public class GesundheitsamtService implements GesundheitsamtInterface {

	@Autowired
	GesundheitsamtRepository gesundheitsamtRepository;
	
	@Autowired
	AdresseRepo adressenRepo;
	
	/* (non-Javadoc)
	 * @see de.adesso.test.services.GesundheitsamtInterface#save(de.adesso.test.entities.Gesundheitsamt)
	 */
	@Override
	public ResponseEntity<Gesundheitsamt> save(Gesundheitsamt gesundheitsamt) {
		List<Adresse> adressenList = new ArrayList<>();
		for (Adresse adresse : gesundheitsamt.getAdresse()) {
			Adresse adr = adressenRepo.save(adresse);
			adressenList.add(adr);
		}
		gesundheitsamt.setAdresse(adressenList);
		Gesundheitsamt createdGesundheitsamt = gesundheitsamtRepository.save(gesundheitsamt);
		return new ResponseEntity<Gesundheitsamt>(createdGesundheitsamt, HttpStatus.CREATED);
	}
	
	/* (non-Javadoc)
	 * @see de.adesso.test.services.GesundheitsamtInterface#getAll()
	 */
	@Override
	public List<Gesundheitsamt> getAll() {
		return Lists.newArrayList(gesundheitsamtRepository.findAll());
	}
	
	/* (non-Javadoc)
	 * @see de.adesso.test.services.GesundheitsamtInterface#update(de.adesso.test.entities.Gesundheitsamt)
	 */
	@Override
	public ResponseEntity<Gesundheitsamt> update(Gesundheitsamt gesundheitsamt) throws Throwable {
		Gesundheitsamt gesundheitsamtToUpdate = gesundheitsamtRepository
				.findOne(gesundheitsamt.getGesundheitsamtId());
		if(gesundheitsamtToUpdate != null) {
			gesundheitsamtRepository.save(gesundheitsamt);
			return new ResponseEntity<Gesundheitsamt>(gesundheitsamt, HttpStatus.CREATED);
		} else {
			throw new Exception("Das Element existiert nicht");
		}
	}
	
	/* (non-Javadoc)
	 * @see de.adesso.test.services.GesundheitsamtInterface#delete(long)
	 */
	@Override
	public ResponseEntity delete(long id) {
		gesundheitsamtRepository.delete(id);
		return new ResponseEntity(HttpStatus.OK);
	}

	@Override
	public List<Gesundheitsamt> findByName(String name) {
		return gesundheitsamtRepository.findByName(name);
	}
	
}
