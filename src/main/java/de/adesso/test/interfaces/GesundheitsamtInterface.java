package de.adesso.test.interfaces;

import java.util.List;

import org.springframework.http.ResponseEntity;

import de.adesso.test.entities.Gesundheitsamt;

public interface GesundheitsamtInterface {

	ResponseEntity<Gesundheitsamt> save(Gesundheitsamt gesundheitsamt);

	List<Gesundheitsamt> getAll();

	ResponseEntity<Gesundheitsamt> update(Gesundheitsamt gesundheitsamt) throws Throwable;

	ResponseEntity delete(long id);
	
	List<Gesundheitsamt> findByName(String name);

}